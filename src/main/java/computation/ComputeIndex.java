package computation;

import entities.DbClient;

import java.util.Collections;
import java.util.List;

public class ComputeIndex {

    private static void printHIndex(String authorName, List<Long> citations) {

        citations.sort(Collections.reverseOrder());

        long hIndex = citations.size();

        long temp = 0;
        for (; hIndex > 0; hIndex--) {
            long finalI = hIndex;
            temp = citations.stream().limit(hIndex).filter(input -> input >= finalI).count();
            if (temp == hIndex) {
                break;
            }
        }

        System.out.println("Author " + authorName + " has hIndex: " + hIndex);

    }

    public static void main(String[] args) {
//        GenericXmlApplicationContext context = new GenericXmlApplicationContext("applicationContext.xml");
//        DbClient dbClient = context.getBean(DbClient.class);
//
//        String authorName = "Utsaw Kumar";
//        int count = dbClient.getPapersForAuthor(authorName);
//        System.out.println("Number papers for author " + authorName + " is: " + count);
//        dbClient.setSessionFactory();


        DbClient dbClient = new DbClient();

        String authorName = "Minos N. Garofalakis";
        printHIndex(authorName, dbClient.getCitationsForAuthor(authorName));
//        System.out.println("Number papers for author " + authorName + " is: " + count);


    }

}
