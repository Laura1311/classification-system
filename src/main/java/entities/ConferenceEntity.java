package entities;

import javax.persistence.*;

/**
 * Created by laura on 6/3/17.
 */
@Entity
@Table(name = "conference", schema = "dblp")
public class ConferenceEntity {
    private String confKey;
    private String name;
    private String detail;

    @Basic
    @Column(name = "conf_key", nullable = true, length = 100)
    public String getConfKey() {
        return confKey;
    }

    public void setConfKey(String confKey) {
        this.confKey = confKey;
    }

    @Basic
    @Column(name = "name", nullable = true, length = -1)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "detail", nullable = true, length = -1)
    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ConferenceEntity that = (ConferenceEntity) o;

        if (confKey != null ? !confKey.equals(that.confKey) : that.confKey != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (detail != null ? !detail.equals(that.detail) : that.detail != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = confKey != null ? confKey.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (detail != null ? detail.hashCode() : 0);
        return result;
    }

    private String id;

    @Id
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
