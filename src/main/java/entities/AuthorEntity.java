package entities;

import javax.persistence.*;

/**
 * Created by laura on 6/3/17.
 */
@Entity
@Table(name = "author", schema = "dblp")
public class AuthorEntity {
    private String name;
    private String paperKey;

    @Basic
    @Column(name = "name", nullable = true, length = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "paper_key", nullable = true, length = 200)
    public String getPaperKey() {
        return paperKey;
    }

    public void setPaperKey(String paperKey) {
        this.paperKey = paperKey;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AuthorEntity that = (AuthorEntity) o;

        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (paperKey != null ? !paperKey.equals(that.paperKey) : that.paperKey != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (paperKey != null ? paperKey.hashCode() : 0);
        return result;
    }

    private String id;

    @Id
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
