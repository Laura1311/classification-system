package entities;

import org.hibernate.HibernateException;
import org.hibernate.Metamodel;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.jpa.HibernateQuery;
import org.hibernate.query.Query;

import javax.persistence.metamodel.EntityType;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by laura on 6/3/17.
 */
public class DbClient {
    private static SessionFactory ourSessionFactory;
    private static Session session;

    //
    static {
        try {
            Configuration configuration = new Configuration();
            configuration.configure();

            ourSessionFactory = configuration.buildSessionFactory();
            session = ourSessionFactory.openSession();
        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }

    public List<String> getPapersForAuthor(String name) {
//        String hql = "select * from "
        Query query = session.createQuery("from PaperEntity p " +
                "join AuthorEntity a  on p.paperKey=a.paperKey " +
                "where a.name=:name");


        query.setParameter("name", name);

        return (List<String>) query.list();
    }


    public List<Long> getCitationsForAuthor(String authorName) {
        Query query1 = session.createQuery("select p.paperKey from PaperEntity p " +
                "join AuthorEntity a  on p.paperKey=a.paperKey " +
                "where a.name=:name");
        query1.setParameter("name", authorName);

        Query query = session.createQuery("select count(paperCitedKey) from CitationEntity c " +
                "where c.paperCitedKey in (:papers) " +
                "group by c.paperCitedKey");
        query.setParameter("papers", query1.list());


        return (List<Long>) query.list();

    }

//    public void setSessionFactory(SessionFactory sessionFactory) {
//        this.ourSessionFactory = sessionFactory;
//        session = ourSessionFactory.openSession();
//    }

//    public Session getSession() throws HibernateException {
//        return ourSessionFactory.openSession();
//    }
//
//    public static void main(final String[] args) throws Exception {
//        final Session session = getSession();
//        try {
//            System.out.println("querying all the managed entities...");
//            final Metamodel metamodel = session.getSessionFactory().getMetamodel();
//            for (EntityType<?> entityType : metamodel.getEntities()) {
//                final String entityName = entityType.getName();
//                final Query query = session.createQuery("from " + entityName);
//                System.out.println("executing: " + query.getQueryString());
//                for (Object o : query.list()) {
//                    System.out.println("  " + o);
//                }
//            }
//        } finally {
//            session.close();
//        }
//    }
}
