package entities;

import javax.persistence.*;

/**
 * Created by laura on 6/3/17.
 */
@Entity
@Table(name = "citation", schema = "dblp")
public class CitationEntity {
    private String paperCiteKey;
    private String paperCitedKey;

    @Basic
    @Column(name = "paper_cite_key", nullable = true, length = 200)
    public String getPaperCiteKey() {
        return paperCiteKey;
    }

    public void setPaperCiteKey(String paperCiteKey) {
        this.paperCiteKey = paperCiteKey;
    }

    @Basic
    @Column(name = "paper_cited_key", nullable = true, length = 200)
    public String getPaperCitedKey() {
        return paperCitedKey;
    }

    public void setPaperCitedKey(String paperCitedKey) {
        this.paperCitedKey = paperCitedKey;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CitationEntity that = (CitationEntity) o;

        if (paperCiteKey != null ? !paperCiteKey.equals(that.paperCiteKey) : that.paperCiteKey != null) return false;
        if (paperCitedKey != null ? !paperCitedKey.equals(that.paperCitedKey) : that.paperCitedKey != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = paperCiteKey != null ? paperCiteKey.hashCode() : 0;
        result = 31 * result + (paperCitedKey != null ? paperCitedKey.hashCode() : 0);
        return result;
    }

    private String id;

    @Id
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
