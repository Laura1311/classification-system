package entities;

import javax.persistence.*;

/**
 * Created by laura on 6/3/17.
 */
@Entity
@Table(name = "paper", schema = "dblp")
public class PaperEntity {
    private String title;
    private Integer year;
    private String conference;
    private String paperKey;

    @Basic
    @Column(name = "title", nullable = true, length = -1)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Basic
    @Column(name = "year", nullable = true)
    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    @Basic
    @Column(name = "conference", nullable = true, length = -1)
    public String getConference() {
        return conference;
    }

    public void setConference(String conference) {
        this.conference = conference;
    }

    @Basic
    @Column(name = "paper_key", nullable = true, length = 200)
    public String getPaperKey() {
        return paperKey;
    }

    public void setPaperKey(String paperKey) {
        this.paperKey = paperKey;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PaperEntity that = (PaperEntity) o;

        if (title != null ? !title.equals(that.title) : that.title != null) return false;
        if (year != null ? !year.equals(that.year) : that.year != null) return false;
        if (conference != null ? !conference.equals(that.conference) : that.conference != null) return false;
        if (paperKey != null ? !paperKey.equals(that.paperKey) : that.paperKey != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = title != null ? title.hashCode() : 0;
        result = 31 * result + (year != null ? year.hashCode() : 0);
        result = 31 * result + (conference != null ? conference.hashCode() : 0);
        result = 31 * result + (paperKey != null ? paperKey.hashCode() : 0);
        return result;
    }

    private String id;

    @Id
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
